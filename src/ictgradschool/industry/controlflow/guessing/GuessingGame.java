package ictgradschool.industry.controlflow.guessing;

import ictgradschool.Keyboard;
import org.omg.CORBA.INTERNAL;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.
        int goal = (int) (Math.random() * 100 + 1);

        int guess = 0;
        System.out.println("Enter your guess (1 – 100)");

        while (guess != goal) {
            guess = Integer.parseInt(Keyboard.readInput());
            if (guess > goal) {
                System.out.println("Too high,try again");
            } else if (guess < goal) {
                System.out.println("Too low,try again");
            } else {
                System.out.println("Perfect!!");
            }
        }
        System.out.println("Goodbye");
    }


    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
