package ictgradschool.industry.controlflow.coderunner;

import ictgradschool.Keyboard;

/**
 * Please run TestCodeRunner to check your answers
 */
public class CodeRunner {
    /**
     * Q1. Compare two names and if they are the same return "Same name",
     * otherwise if they start with exactly the same letter return "Same
     * first letter", otherwise return "No match".
     *
     * @param firstName
     * @param secondName
     * @return one of three Strings: "Same name", "Same first letter",
     * "No match"
     */
    public String areSameName(String firstName, String secondName) {
        String message = "";
        // TODO write answer to Q1
        if (firstName.equals(secondName)) {
            message = "Same name";
        } else if (firstName.substring(0, 1).equals(secondName.substring(0, 1))) {
            message = "Same first letter";
        } else {
            message = "No match";
        }
        return message;
    }
    /** areSameName(String, String) => String **/


    /**
     * Q2. Determine if the given year is a leap year.
     *
     * @param year
     * @return true if the given year is a leap year, false otherwise
     */
    public boolean isALeapYear(int year) {
        boolean leapYear = false;
        // TODO write answer for Q2

        if (year % 4 == 0 && !(year % 100 == 0 && year % 400 != 0)) {
            leapYear = true;
        } else {
            leapYear = false;
        }
        return leapYear;
    }
    /** isALeapYear(int) => boolean **/


    /**
     * Q3. When given an integer, return an integer that is the reverse (its
     * numbers are in reverse to the input).
     * order.
     *
     * @param number
     * @return the integer with digits in reverse order
     */
    public int reverseInt(int number) {
        int reverseNum = 0;
        // TODO write answer for Q3
        boolean isNegative = false;

        if (number < 0) {
            isNegative = true;
        }

        number = Math.abs(number);

        while (number != 0) {
            int one = number % 10;
            number /= 10;
            reverseNum = reverseNum * 10;
            reverseNum = reverseNum + one;
        }

        if (isNegative) {
            reverseNum = reverseNum * -1;
        }

        return reverseNum;
    }


    /**
     * Q4. Return the given string in reverse order.
     *
     * @param str
     * @return the String with characters in reverse order
     */
    public String reverseString(String str) {
        String reverseStr = "";
        // TODO write answer for Q4


        int i = 0;
        while (i < str.length()) {
            System.out.println(i);
            reverseStr = str.charAt(i) + reverseStr;
            i++;
        }

        return reverseStr;
    }
    /** reverseString(String) => void **/


    /**
     * Q5. Generates the simple multiplication table for the given integer.
     * The resulting table should be 'num' columns wide and 'num' rows tall.
     *
     * @param num
     * @return the multiplication table as a newline separated String
     */
    public String simpleMultiplicationTable(int num) {
        String multiplicationTable = "";
        // TODO write answer for Q5

        for (int i = 1; i <= num; i++) {

            for (int j = 1; j <= num; j++) {
                multiplicationTable = multiplicationTable + i * j + " ";
            }
            multiplicationTable = multiplicationTable.trim();

            if (i != num) {
                multiplicationTable = multiplicationTable + "\n";
            }
        }
        return multiplicationTable;
    }
    /** simpleMultiplicationTable(int) => void **/


    /**
     * Q6. Determines the Excel column name of the given column number.
     *
     * @param num
     * @return the column title as a String
     */
    public String convertIntToColTitle(int num) {
        String columnName = "";
        // TODO write answer for Q6

        while (num > 0) {
            char temp = (char) ('A' + (num - 1) % 26);
            num = (num - 1) / 26;
            columnName = temp + columnName;

        }

        if (columnName.isEmpty()) {
            columnName = "Input is invalid";
        }
//        if(num>0){
//            int a = 1;
//            char b = (char) a;
//        }
//        else {
//            return "Input is invalid";
//        }

        return columnName;
    }
    /** convertIntToColTitle(int) => void **/


    /**
     * Q7. Determine if the given number is a prime number.
     *
     * @param num
     * @return true is the given number is a prime, false otherwise
     */
    public boolean isPrime(int num) {
        // TODO write answer for Q7

        boolean isPrime = false;
        for (int i = 2; i <= num; i++) {
            isPrime = true;
            for (int j = 2; j < i; j++) {

                if ((i % j) == 0) {
                    isPrime = false;
                    break;
                }
            }
        }


        return isPrime;
    }
    /** isPrime(int) => void **/


    /**
     * Q8. Determine if the given integer is a palindrome (ignoring negative
     * sign).
     *
     * @param num
     * @return true is int is palindrome, false otherwise
     */
    public boolean isIntPalindrome(int num) {
        // TODO write answer for Q8
        return num == reverseInt(num);
//        boolean isIntPalindrome
//            if (x < 0) return false;
//            if (x == 0) return true;
//            if (isPalindrome(x/10, y) && (x%10 == y%10)) {
//                y /= 10;
//                return true;
//            } else {
//                return false;
//            }
//        }
//        boolean isIntPalindrome(int x) {
//            return isIntPalindrome(x, x);
//        }

    }
    /** isIntPalindrom(int) => boolean **/


    /**
     * Q9. Determine if the given string is a palindrome (case folded).
     *
     * @param str
     * @return true if string is palindrome, false otherwise
     */
    public boolean isStringPalindrome(String str) {
        // TODO write answer for Q9
        String noSpace = str.replaceAll(" ", "");
        return noSpace.equals(reverseString(noSpace));
//        public class Palindrome
//        {
//            public static void main(String args[])
//            {
//                String a, b = "";
//
//                System.out.print("Enter the string you want to check:");
//
//                int n = a.length();
//                for(int i = n - 1; i >= 0; i--)
//                {
//                    b = b + a.charAt(i);
//                }
//                if(a.equalsIgnoreCase(b))
//                {
//                    System.out.println("The string is palindrome.");
//                }
//                else
//                {
//                    System.out.println("The string is not palindrome.");
//                }
//            }
//        }


    }
    /** isStringPalindrome(String) => boolean **/


    /**
     * Q10. Generate a space separated list of all the prime numbers from 2
     * to the highest prime less than or equal to the given integer.
     *
     * @param num
     * @return the primes as a space separated list
     */
    public String printPrimeNumbers(int num) {
        String primesStr = "";
        // TODO write answer for Q10

        for (int i = 2; i <= num; i++) {
            if(isPrime(i)){
                primesStr = primesStr + i + " ";
            }
        }

        if(primesStr.isEmpty()){
            primesStr = "No prime number found";
        }
        primesStr = primesStr.trim();
        return primesStr;

//        public class printPrimeNumbers {
//            public static void main(String[] args) {
//
//                for(int i=1;i<=n;i++){
//                    if(isPrime(i)){
//                        System.out.println(i);
//                    }
//                }
//            }
//            public static boolean isPrime(int a) {
//                boolean flag = true;
//
//                if (a < 2) {
//                    return false;
//                } else {
//                    for (int i = 2; i <= Math.sqrt(a); i++) {
//
//                        if (a % i == 0) {
//                            flag = false;
//                            break;
//                        }
//                    }
//                }
//                return flag;
//            }
//        }
        
    }
}
